package book

import "time"

type Book struct {
	ID         int
	Unit_Area  string
	Nama_Prshn string
	No_Ktp     int
	Direktur   string
	Dir_Wakil  string
	Hp_Dir     int
	Hp_Wadir   int
	Tlp_Prshn  int
	Email      string
	Kota       string
	Alamat     string
	Siup       int
	Siup_Tgl   string
	Website    string
	Bidang     string
	Status     string

	CreatedAt time.Time
	UpdatedAt time.Time
}
