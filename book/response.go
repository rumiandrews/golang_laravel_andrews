package book

type BookResponse struct {
	ID         int    `json:"ID"`
	Unit_Area  string `json:"Unit Area"`
	Nama_Prshn string `json:"Nama Perusahaan"`
	No_Ktp     int    `json:"No Ktp"`
	Direktur   string `json:"Direktur"`
	Dir_Wakil  string `json:"Direktur Wakil"`
	Hp_Dir     int    `json:"Hp Direktur"`
	Hp_Wadir   int    `json:"Hp Direktur Wakil"`
	Tlp_Prshn  int    `json:"Telepon Perusahaan"`
	Email      string `json:"Email"`
	Kota       string `json:"Kota"`
	Alamat     string `json:"Alamat"`
	Siup       int    `json:"Siup No"`
	Siup_Tgl   string `json:"Siup Tanggal"`
	Website    string `json:"Website"`
	Bidang     string `json:"Bidang Usaha"`
	Status     string `json:"Status"`
}
