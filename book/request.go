package book

import "encoding/json"

type BookRequest struct {
	Unit_Area  string      `json:"Unit Area" binding:"required"`
	Nama_Prshn string      `json:"Nama Perusahaan" binding:"required"`
	No_Ktp     json.Number `json:"No Ktp" binding:"required,number"`
	Direktur   string      `json:"Direktur" binding:"required"`
	Dir_Wakil  string      `json:"Direktur Wakil" binding:"required"`
	Hp_Dir     json.Number `json:"Hp Direktur" binding:"required,number"`
	Hp_Wadir   json.Number `json:"Hp Direktur Wakil" binding:"required,number"`
	Tlp_Prshn  json.Number `json:"Telepon Perusahaan" binding:"required,number"`
	Email      string      `json:"Email" binding:"required,email"`
	Kota       string      `json:"Kota" binding:"required"`
	Alamat     string      `json:"Alamat" binding:"required"`
	Siup       json.Number `json:"Siup No" binding:"required,number"`
	Siup_Tgl   string      `json:"Siup Tanggal" binding:"required"`
	Website    string      `json:"Website" binding:"required"`
	Bidang     string      `json:"Bidang Usaha" binding:"required"`
	Status     string      `json:"Status" binding:"required"`
}
