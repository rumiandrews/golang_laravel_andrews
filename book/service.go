package book

type Service interface {
	FindAll() ([]Book, error)
	FindByID(ID int) (Book, error)
	Create(bookRequest BookRequest) (Book, error)
	Update(ID int, bookRequest BookRequest) (Book, error)
	Delete(ID int) (Book, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) FindAll() ([]Book, error) {
	books, err := s.repository.FindAll()
	return books, err
}

func (s *service) FindByID(ID int) (Book, error) {
	book, err := s.repository.FindByID(ID)
	return book, err
}

func (s *service) Create(bookRequest BookRequest) (Book, error) {
	noKTP, _ := bookRequest.No_Ktp.Int64()
	hpDIR, _ := bookRequest.Hp_Dir.Int64()
	hpWADIR, _ := bookRequest.Hp_Wadir.Int64()
	tlpPRSHN, _ := bookRequest.Tlp_Prshn.Int64()
	siupNO, _ := bookRequest.Siup.Int64()

	book := Book{
		Unit_Area:  bookRequest.Unit_Area,
		Nama_Prshn: bookRequest.Nama_Prshn,
		No_Ktp:     int(noKTP),
		Direktur:   bookRequest.Direktur,
		Dir_Wakil:  bookRequest.Dir_Wakil,
		Hp_Dir:     int(hpDIR),
		Hp_Wadir:   int(hpWADIR),
		Tlp_Prshn:  int(tlpPRSHN),
		Email:      bookRequest.Email,
		Kota:       bookRequest.Kota,
		Alamat:     bookRequest.Alamat,
		Siup:       int(siupNO),
		Siup_Tgl:   bookRequest.Siup_Tgl,
		Website:    bookRequest.Website,
		Bidang:     bookRequest.Bidang,
		Status:     bookRequest.Status,
	}

	newBook, err := s.repository.Create(book)
	return newBook, err
}

func (s *service) Update(ID int, bookRequest BookRequest) (Book, error) {
	book, err := s.repository.FindByID(ID)

	noKTP, _ := bookRequest.No_Ktp.Int64()
	tlpPRSHN, _ := bookRequest.Tlp_Prshn.Int64()
	siupNO, _ := bookRequest.Siup.Int64()

	book.Unit_Area = bookRequest.Unit_Area
	book.Nama_Prshn = bookRequest.Nama_Prshn
	book.No_Ktp = int(noKTP)
	book.Direktur = bookRequest.Direktur
	book.Dir_Wakil = bookRequest.Dir_Wakil
	book.Tlp_Prshn = int(tlpPRSHN)
	book.Email = bookRequest.Email
	book.Kota = bookRequest.Kota
	book.Alamat = bookRequest.Alamat
	book.Siup = int(siupNO)
	book.Siup_Tgl = bookRequest.Siup_Tgl
	book.Website = bookRequest.Website
	book.Bidang = bookRequest.Bidang
	book.Status = bookRequest.Status

	newBook, err := s.repository.Update(book)
	return newBook, err
}

func (s *service) Delete(ID int) (Book, error) {
	book, err := s.repository.FindByID(ID)
	newBook, err := s.repository.Delete(book)
	return newBook, err
}
